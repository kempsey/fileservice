﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using SIO = System.IO;
using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;

namespace SourceCode.SmartObjects.Services
{
    /// <summary>
    /// Version 2 implementation of a Static File Service Object, based on Johnny's original File Server Broker implementation.
    /// Added methods to Read a file, Delete a file and list files in a directory.
    /// v.2.0 jamesk 3 Aug 2016
    /// The class is decorated with a ServiceObject Attribute providing definition information for the Service Object.
    /// The Properties and Methods in the class are each decorated with attributes that describe them in Service Object terms
    /// This sample implementation contains two Properties (Number and Text) and two Methods (Read and List)
    /// </summary>
    [Attributes.ServiceObject("FileSystemService", "File System Service", "File System Service")]
    class FileService
    {
	
		/// <summary>
        /// This property is required if you want to get the service instance configuration 
        /// settings in this class
        /// </summary>
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }
		
        #region Class Level Fields

        #region Private Fields
        private string pFileData = string.Empty;
        private string pFileName = string.Empty;
        private string pFilePath = string.Empty;
		private string pFileDataString = string.Empty;
        private bool pOverwriteFile = false;
        private bool pRecursive= false;
       
        #endregion

        #endregion

        #region Properties with Property Attribute

        #region string File
        /// <summary>
        /// The File object representing the file being uploaded.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("FileData", SoType.File, "File Data", "File Data")]
        public string FileData
        {
            get { return pFileData; }
            set { pFileData = value; }
        }

		[Attributes.Property("FileDataString", SoType.Text, "File Data String", "File Data String")]
		public string FileDataString
		{
			get { return pFileDataString; }
			set { pFileDataString = value; }
		}
		#endregion

		#region string FileName
		/// <summary>
		/// An string value representing the desired file name.
		/// The property is decorated with a Property Attribute which describes the Service Object Property
		/// </summary>
		[Attributes.Property("FileName", SoType.Text, "File Name", "File Name")]
        public string FileName
        {
            get { return pFileName; }
            set { pFileName = value; }
        }
        #endregion

        #region string FilePath
        /// <summary>
        /// An string value representing the uploaded location file path.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("FilePath", SoType.Text, "File Path", "File Path")]
        public string FilePath
        {
            get { return pFilePath; }
            set { pFilePath = value; }
        }
        #endregion
               

        #region bool OverwriteFile
        /// <summary>
        /// An flag to indicate that the file should be overwritten if it exists. 
        /// Actual function would to to delete and create a new file.
        /// Used in Create method.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("OverwriteFile", SoType.YesNo, "Overwrite File", "Set this flag if the file should be overwritten if exists")]
        public bool OverwriteFile
        {
            get { return pOverwriteFile; }
            set { pOverwriteFile = value; }
        }
        #endregion

        #region bool Recursive
        /// <summary>
        /// An flag to indicate that the list results should recurse sub-directries. 
        /// Used in List methods.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("Recursive", SoType.YesNo, "Recursive", "Set this flag to get data from sub-directories")]
        public bool Recursive
        {
            get { return pRecursive; }
            set { pRecursive = value; }
        }
        #endregion
               
        #endregion


        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public FileService()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region FileService Create(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
        /// <summary>
        /// This method writes a file based to the specified File Path (folder name)
        /// If write is successful, the full path of the file is returned.
        /// If write is unsuccessful, (e.g. because file already exists), a null value is returned as filename.
        /// To overwrite an existing file, set OverwriteFile to true. 
        /// If overwrite file is set, the method will delete the existing file before creating the new file.
        /// The method is decorated with a Method Attribute which describes the Service Object Method.
        /// </summary>
        [Attributes.Method("Create", MethodType.Create, "Create", "Stores a file",
            new string[] { "FileData", "FilePath" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileData", "FilePath", "FileName", "OverwriteFile" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileName" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public FileService Create()
        {
            try
            {
                // Parse original file name and also document base64 string
                XDocument xDoc = new XDocument();
                xDoc = XDocument.Parse(this.FileData);
                string sFileName = xDoc.Root.Element("name").Value;
                byte[] bFile = Convert.FromBase64String(xDoc.Root.Element("content").Value);

                string sFullPath = this.FilePath;

                // append a slash if it is missing
                if (!sFullPath.EndsWith(@"\"))
                {
                    sFullPath += @"\";
                }

                // If root path doesn't exists, attempt to create it.
                if (!SIO.Directory.Exists(sFullPath))
                {
                    SIO.Directory.CreateDirectory(sFullPath);
                }

                

                if (FileName == string.Empty || FileName == null)
                {
                    sFullPath = sFullPath + sFileName;
                }
                else
                {
                    sFileName = FileName; ; 
                    if (FileName.Contains("\\"))
                    {
                        //filname likely contains the full path, retrieve the filename portion from the full path
                        sFileName = FileName.Substring((FileName.LastIndexOf('\\') + 1), (FileName.Length - (FileName.LastIndexOf('\\') + 1)));
                    }

                    sFullPath = sFullPath + sFileName;
                }


                if (SIO.File.Exists(sFullPath))
                {
                    if (OverwriteFile == true)
                    {
                        //delete the file if it exists
                        SIO.File.Delete(sFullPath);
                    }
                    else
                    {
                        //do not delete.return null to indicate operation failed
                        return null;
                    }
                }

                //write file to path            
                SIO.File.WriteAllBytes(sFullPath, bFile);
                // return the full path
                this.FileName  = sFullPath;
                return this;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region FileService Delete(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
        /// <summary>
        /// This method deletes the file specified by the file path.
        /// There is no return value.
        /// The method is decorated with a Method Attribute which describes the Service Object Method.
        /// </summary>
        [Attributes.Method("Delete", MethodType.Delete, "Delete", "Deletes a file",
            new string[] { "FileName" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileName" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public void Delete()
        {
            try
            {

                string sFullPath = this.FileName;
                
                if (SIO.File.Exists(sFullPath))
                {
                        //delete the file if it exists
                        SIO.File.Delete(sFullPath);                
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
		#endregion

		#region FileService CreateFromString(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
		[Attributes.Method("CreateFromString", MethodType.Create, "CreateFromString", "Stores a file",
			new string[] { "FileDataString", "FilePath" },
			new string[] { "FileDataString", "FilePath", "FileName", "OverwriteFile" },
			new string[] { "FileName" })]
		public FileService CreateFromString()
		{
			try
			{
				string sFileName = this.FileName;
				byte[] bFile = Convert.FromBase64String(this.FileDataString);

				string sFullPath = this.FilePath;

				// append a slash if it is missing
				if (!sFullPath.EndsWith(@"\"))
				{
					sFullPath += @"\";
				}

				// If root path doesn't exists, attempt to create it.
				if (!SIO.Directory.Exists(sFullPath))
				{
					SIO.Directory.CreateDirectory(sFullPath);
				}

				if (FileName == string.Empty || FileName == null)
				{
					sFullPath = sFullPath + sFileName;
				}
				else
				{
					sFileName = FileName; ;
					if (FileName.Contains("\\"))
					{
						//filname likely contains the full path, retrieve the filename portion from the full path
						sFileName = FileName.Substring((FileName.LastIndexOf('\\') + 1), (FileName.Length - (FileName.LastIndexOf('\\') + 1)));
					}

					sFullPath = sFullPath + sFileName;
				}


				if (SIO.File.Exists(sFullPath))
				{
					if (OverwriteFile == true)
					{
						//delete the file if it exists
						SIO.File.Delete(sFullPath);
					}
					else
					{
						//do not delete.return null to indicate operation failed
						return null;
					}
				}

				//write file to path            
				SIO.File.WriteAllBytes(sFullPath, bFile);
				// return the full path
				this.FileName = sFullPath;
				return this;
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}
		#endregion



		#region FileService Read(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
		/// <summary>
		/// This method reads a file based on the full file path specified.
		/// If file is not found, this method will return null.
		/// The method is decorated with a Method Attribute which describes the Service Object Method.
		/// </summary>
		[Attributes.Method("Read", MethodType.Read, "Read", "Read from a file",
            new string[] { "FileName", }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileName", }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileData" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public FileService Read()
        {
            try
            {
                string sFullPath = this.FileName;

                // If root path doesn't exists, attempt to create it.
                if (!SIO.File.Exists(sFullPath))
                {
                    return null;
                }

                byte[] bFile = SIO.File.ReadAllBytes(sFullPath);
                
                //retrieve the filename portion from the full path
                FileName = sFullPath.Substring((sFullPath.LastIndexOf('\\') + 1), (sFullPath.Length - (sFullPath.LastIndexOf('\\')+1)));

                StringBuilder sb = new StringBuilder();
                XmlWriterSettings xSettings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = true
                };

                XmlWriter xWriter = XmlWriter.Create(sb, xSettings);
                xWriter.WriteStartDocument();
                xWriter.WriteStartElement("file");
                xWriter.WriteStartElement("name");
                xWriter.WriteString(FileName);
                xWriter.WriteEndElement();
                xWriter.WriteStartElement("content");
                xWriter.WriteBase64(bFile, 0, bFile.Length);
                xWriter.WriteEndElement();
                xWriter.WriteEndDocument();
                xWriter.Close();
                // return the file in base64encoded string
                string sFile = sb.ToString();
                this.FileData = sFile;
                return this;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FileService GetFiles(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
        /// <summary>
        /// This method reads the and returns the full path of the files found in this folder.
        /// If Recursive is set to true, it will also return the files found in the sub-directories in a recursive manner.
        /// The method is decorated with a Method Attribute which describes the Service Object Method.
        /// </summary>
        [Attributes.Method("GetFiles", MethodType.List, "Get Files", "Get list of filenames from a directory",
            new string[] { "FilePath" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FilePath", "Recursive"}, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileName" })] //return  array. Properties must match the names of Properties decorated with the Property Attribute
        public FileService[] GetFiles()
        {
            //string sFileName = this.FileName;
            List<FileService> lstFileServices = new List<FileService>();    
            
            string sBasePath = this.FilePath;
            
            // append a slash if it is missing
            if (!sBasePath.EndsWith(@"\"))
            {
                sBasePath += @"\";
            }

            // If root path doesn't exists, attempt to create it.
            if (!SIO.Directory.Exists(sBasePath))
            {
                return null;
            }

            string[] sDirFileNames;

            if (Recursive == true)
            {
                sDirFileNames = SIO.Directory.GetFiles(sBasePath,"*",SIO.SearchOption.AllDirectories);
            }
            else
            {
                sDirFileNames = SIO.Directory.GetFiles(sBasePath);
            }

            foreach (string filename in sDirFileNames)
            {
                FileService fs = new FileService();
                fs.FileName = filename;
                lstFileServices.Add(fs);
            }

            return lstFileServices.ToArray();
        }
        #endregion

        #region FileService GetDirectories(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
        /// <summary>
        /// This method reads the and returns the full path of the sub-directories under this folder.
        /// If Recursive is set to true, it will also return the sub-directories in the sub-directories in a recursive manner.
        /// The method is decorated with a Method Attribute which describes the Service Object Method.
        /// </summary>
        [Attributes.Method("GetDirectories", MethodType.List, "Get Directories", "Get list of sub-directories from a directory",
            new string[] { "FilePath" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FilePath", "Recursive" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "FileName" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public FileService[] GetDirectories()
        {
            //string sFileName = this.FileName;
            List<FileService> lstFileServices = new List<FileService>();

            string sBasePath = this.FilePath;
            
            // append a slash if it is missing
            if (!sBasePath.EndsWith(@"\"))
            {
                sBasePath += @"\";
            }

            // If root path doesn't exists, attempt to create it.
            if (!SIO.Directory.Exists(sBasePath))
            {
                return null;
            }

            string[] sDirFileNames;

            if (Recursive == true)
            {
                sDirFileNames = SIO.Directory.GetDirectories(sBasePath, "*", SIO.SearchOption.AllDirectories);
            }
            else
            {
                sDirFileNames = SIO.Directory.GetDirectories(sBasePath);
            }

            foreach (string filename in sDirFileNames)
            {
                FileService fs = new FileService();
                fs.FileName = filename;
                lstFileServices.Add(fs);
            }

            return lstFileServices.ToArray();
        }
        #endregion



        #endregion
    }
}